// const { getValidationMessage } = require('../helpers/validation');
const { Load } = require('../models/Load');
const { Truck, getTruckTypeByLoadParams } = require('../models/Truck');
const { User } = require('../models/User');
// const { AddLoadValidation } = require('../validations/validations');

const getLoads = async (req, res) => {
    const { userId } = req.user;
    const { status, limit = 20, offset: skip = 0 } = req.query;
    const user = await User.findById(userId);

    if (user.role === "DRIVER") {
        await user.populate({
            path: "assignedLoads"
        });

        return res.json({ loads: user.assignedLoads });
    }

    if (user.role === "SHIPPER") {
        const match = status ? { status } : null;
        await user.populate({
            path: 'loads',
            skip,
            limit,
            match
        });

        return res.json({ loads: user.loads });
    }

    return res.status(401);
}

const addLoad = async (req, res) => {
    // const { error, value } = AddLoadValidation.validate(req.body, { abortEarly: false });

    // if (error) {
    //     return res.status(400).send({ message: getValidationMessage(error) });
    // }

    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

    const load = new Load({
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        created_by: req.user.userId,
        status: "NEW",
        created_date: new Date().toISOString(),
    });

    await load.save();

    return res.json({ message: 'Load created successfully' });
};

const postLoad = async (req, res) => {
    const { userId } = req.user;
    const load = await Load.findOne({
        _id: req.params.id,
        created_by: userId,
        status: 'NEW'
    });

    if (!load) {
        return res.status(400).json({
            message: `You do not have load with id ${req.params.id} or it already posted`
        })
    }

    const truckType = getTruckTypeByLoadParams(load);

    if (truckType) {
        load.status = "POSTED";

        const truck = await Truck.findOne({
            status: 'IS',
            type: truckType,
            assigned_to: {
                $exists: true
            }
        });

        if (truck) {
            load.assigned_to = truck.assigned_to;
            load.status = 'ASSIGNED';
            load.state = 'En route to Pick Up';
            load.logs = [
                ...load.logs,
                {
                    message: `Load assigned to driver with id ${truck.assigned_to}`,
                    time: new Date().toISOString()
                }
            ]

            await load.save();

            truck.status = 'OL';

            await truck.save();

            return res.json({ message: 'Load posted successfully', driver_found: true });
        }
    }

    load.status = "NEW";

    load.logs = [
        ...load.logs,
        {
            message: `Truck is not found`,
            time: new Date().toISOString()
        }
    ]

    await load.save();

    return res.json({ message: 'Load have not posted', driver_found: false });
}

const getActiveLoad = (req, res) => {
    return Load.findOne({ assigned_to: req.user.userId })
        .then((load) => res.json({
            load
        }))
        .catch(() => {
            res.status(500).send({ message: 'Something went wrong...' });
        });
};

const iterateLoadState = async (req, res) => {
    const load = await Load.findOne({ assigned_to: req.user.userId, status: "ASSIGNED" });

    if (!load) {
        return res.status(400).json({
            message: "You do not have active loads"
        })
    }

    if (load.state === "En route to Pick Up") {
        load.state = "Arrived to Pick Up"
    } else if (load.state === "Arrived to Pick Up") {
        load.state = "En route to delivery"
    } else if (load.state === "En route to delivery") {
        load.state = "Arrived to delivery";
        load.status = 'SHIPPED';

        const truck = await Truck.findOne({
            assigned_to: req.user.userId,
            status: 'OL'
        });

        truck.status = "IS";

        await truck.save();
    }

    return load.save()
        .then((load) => res.json({ message: `Load state changed to '${load.state}'` }))
        .catch(() => {
            res.status(500).send({ message: 'Something went wrong...' });
        });
}

const getLoad = async (req, res) => {
    const load = await Load.findOne({
        created_by: req.user.userId,
        _id: req.params.id
    });

    if (!load) {
        return res.status(400).json({
            message: `You do not have load with id ${req.params.id}`
        })
    }

    return res.json({
        load,
    })
}

const updateLoad = async (req, res) => {
    const load = await Load.findOne({
        created_by: userId,
        _id: req.params.id
    });

    if (!load) {
        return res.status(400).json({
            message: `You do not have load with id ${req.params.id}`
        })
    }

    const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    } = req.body;

    if (name) load.name = name;
    if (payload) load.payload = payload;
    if (pickup_address) load.pickup_address = pickup_address;
    if (delivery_address) load.delivery_address = delivery_address;
    if (dimensions) load.dimensions = dimensions;

    return load.save()
        .then(() => res.json({ message: 'Load details changed successfully' }))
        .catch(() => {
            res.status(400).send({ message: 'Something went wrong...' });
        });
}

const deleteLoad = async (req, res) => {
    const { userId } = req.user;
    const load = await Load.findOne({
        created_by: userId,
        _id: req.params.id
    });

    if (!load) {
        return res.status(400).json({
            message: `You do not have load with id ${req.params.id}`
        })
    }

    Load.findOneAndDelete({
        created_by: userId,
        _id: req.params.id
    })
        .then(() => res.json({ message: 'Load deleted successfully' }))
        .catch(() => {
            res.status(400).send({ message: 'Something went wrong...' });
        })
}

const getShipingInfo = async (req, res) => {
    const load = await Load.findOne({
        _id: req.params.id,
        created_by: req.user.userId,
        status: {
            $ne: "SHIPPED"
        }
    });

    if (load) {
        const truck = await Truck.findOne({
            assigned_to: load.assigned_to
        });

        return res.json({
            load,
            truck
        });
    }

    return res.status(400).json({
        message: "Load shipped"
    })
}

module.exports = {
    getLoads,
    addLoad,
    deleteLoad,
    postLoad,
    getActiveLoad,
    iterateLoadState,
    getLoad,
    updateLoad,
    getShipingInfo,
};
