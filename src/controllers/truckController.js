const { Truck } = require('../models/Truck');

const addTruck = (req, res) => {
  const { type } = req.body;
  const { userId } = req.user;

  const truck = new Truck({
    type,
    created_by: userId,
    status: "IS",
    created_date: new Date().toISOString(),
  });

  truck.save()
    .then(() => {
      res.json({ message: 'Truck created successfully' });
    })
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

const getTrucks = (req, res) => {
  return Truck.find({
    created_by: req.user.userId
  })
    .then((trucks) => res.json({ trucks }))
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
}

const getTruck = async (req, res) => {
  const truck = await Truck.findOne({
    _id: req.params.id,
    created_by: req.user.userId
  });

  if (!truck) {
    return res.status(404).json({
      message: "Truck not found"
    });
  }

  return res.json({ truck });
}

const updateTruck = async (req, res) => {
  const { type } = req.body;

  const truck = await Truck.findOne({
    _id: req.params.id,
    created_by: req.user.userId
  });

  if (!truck) {
    return res.status(404).json({
      message: "Truck not found"
    });
  }

  if (type) truck.type = type;

  return truck.save()
    .then(() => res.json({ message: 'Truck details changed successfully' }))
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
}

const deleteTruck = async (req, res) => {
  const truck = await Truck.findOne({
    _id: req.params.id,
    created_by: req.user.userId
  });

  if (!truck) {
    return res.status(404).json({
      message: "Truck not found"
    });
  }

  Truck.findByIdAndDelete(req.params.id)
    .then(() => res.json({ message: 'Truck deleted successfully' }))
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    })
}

const assignTruck = async (req, res) => {
  const driverHasAssignedTruck = await Truck.findOne({
    assigned_to: req.user.userId
  });

  if (driverHasAssignedTruck) {
    return res.status(400).json({
      message: "Tuck is not assigned, because driver already has assigned truck"
    })
  }

  const truck = await Truck.findOne({
    _id: req.params.id,
    created_by: req.user.userId
  });

  if (!truck) {
    return res.status(404).json({
      message: "Truck not found"
    });
  }

  truck.assigned_to = req.user.userId;

  return truck.save()
    .then(() => res.json({ message: 'Truck assigned successfully' }))
    .catch(() => {
      res.status(500).send({ message: 'Something went wrong...' });
    });
}

module.exports = {
  addTruck,
  getTrucks,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
