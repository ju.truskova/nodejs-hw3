const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { getErrorMessage } = require('../helpers/errors');
// const { getValidationMessage } = require('../helpers/validation');
// const { LoginValidation } = require('../validations/validations');

const { User,
  // UserValidation
} = require('../models/User');

const registerUser = async (req, res) => {
  // const { error, value } = UserValidation.validate(req.body, { abortEarly: false });
  // const { email, password, role } = value;

  // if (error) {
  //   return res.status(400).send({ message: getValidationMessage(error) });
  // }

  const { email, password, role } = req.body;

  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 1),
    created_date: new Date().toISOString(),
  });

  user
    .save()
    .then(() => {
      return res.json({ message: 'Profile created successfully' });
    })
    .catch((error) => {
      res.status(500).send({ message: getErrorMessage(error) });
    });
};

const loginUser = async (req, res) => {
  // const { error, value } = LoginValidation.validate(req.body);

  // if (error) {
  //   return res.status(400).send({ message: getValidationMessage(error) });
  // }

  const { email, password } = req.body;

  const user = await User.findOne({ email }, '+password');

  if (
    user
    && (await bcrypt.compare(String(password), String(user.password)))
  ) {
    const payload = { email: user.email, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');

    return res.json({
      message: 'You logged in successfully',
      jwt_token: jwtToken,
    });
  }

  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
