const Joi = require('Joi');

const LoginValidation = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
});

const AddLoadValidation = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
        width: Joi.number().required(),
        length: Joi.number().required(),
        height: Joi.number().required(),
    }).required()
});

module.exports = {
    LoginValidation,
    AddLoadValidation
}
