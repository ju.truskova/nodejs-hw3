const mongoose = require('mongoose');

const TruckPayload = {
  SPRINTER: 1700,
  "SMALL STRAIGHT": 2500,
  "LARGE STRAIGHT": 4000,
}

const TruckDimensions = {
  SPRINTER: {
    width: 170,
    length: 300,
    height: 250
  },
  "SMALL STRAIGHT": {
    width: 170,
    length: 500,
    height: 250
  },
  "LARGE STRAIGHT": {
    width: 200,
    length: 700,
    height: 350
  }
}

const getTruckTypeByLoadParams = (load) => {
  const [type] = Object.entries(TruckPayload).sort(([, a], [, b]) => {
    return b > a ? -1 : 1
  }).find(([_, payload]) => {
    return payload > load.payload
  });

  if (!type) {
    return undefined;
  }

  const dimensions = TruckDimensions[type];

  const isDimensionsOk = Object.entries(dimensions).reduce((acc, [key, dimension]) => {
    if (!acc) {
      return false;
    };

    return dimension > load.dimensions[key];
  }, true);

  return isDimensionsOk ? type : undefined;
}

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
  },
  type: {
    type: String,
    required: true,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
  },
  status: {
    type: String,
    enum: ["OL", "IS"],
  },
  created_date: {
    type: String,
    required: false,
  },
});

module.exports = {
  Truck,
  TruckDimensions,
  getTruckTypeByLoadParams
};
