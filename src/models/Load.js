const mongoose = require('mongoose');

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  status: {
    type: String,
    enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
  },
  state: {
    type: String,
    enum: ["En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"],
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    width: { type: String },
    length: { type: String },
    height: { type: String },
  },
  logs: [{
    message: String,
    time: String
  }],
  created_date: {
    type: String,
  }
});

const Load = mongoose.model('Load', LoadSchema);

module.exports = {
  Load,
};
