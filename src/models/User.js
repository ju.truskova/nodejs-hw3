// const Joi = require('Joi');
const mongoose = require('mongoose');

// const UserValidation = Joi.object({
//   email: Joi.string().email().required(),
//   password: Joi.string().required(),
//   role: Joi.string().valid("SHIPPER", "DRIVER").required()
// });

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    unique: true,
    select: false
  },
  role: {
    type: String,
    required: true,
    enum: ["SHIPPER", "DRIVER"],
  },
  created_date: {
    type: String,
    required: false,
  }
});

UserSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'created_by'
});

UserSchema.virtual('assignedLoads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'assigned_to'
});

const User = mongoose.model('User', UserSchema);

module.exports = {
  User,
  // UserValidation
};
