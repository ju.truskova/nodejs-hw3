const getErrorMessage = (error) => {
    let message = "Something went wrong";

    if (error && error.message) {
        message = error.message;
    }

    return message;
}

module.exports = {
    getErrorMessage
}
