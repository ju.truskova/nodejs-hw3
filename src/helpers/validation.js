const getValidationMessage = (error) => {
    return error.details.reduce((acc, { message }) => {
        return acc + ', ' + message;
    }, '')
};

module.exports = {
    getValidationMessage
}
