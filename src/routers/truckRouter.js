const express = require('express');
const { authMiddleware } = require('../middleware/authMiddleware');
const { driverMiddleware } = require("../middleware/driverMiddleware");
const { addTruck, getTrucks, getTruck, updateTruck, deleteTruck, assignTruck } = require('../controllers/truckController');

const router = express.Router();

router.use(authMiddleware, driverMiddleware);

router.post('/', addTruck);

router.get('/', getTrucks);

router.get('/:id', getTruck);

router.put('/:id', updateTruck);

router.delete('/:id', deleteTruck);

router.post('/:id/assign', assignTruck);

module.exports = {
  truckRouter: router,
};
