const express = require('express');

const {
  getLoads,
  addLoad,
  deleteLoad,
  postLoad,
  getActiveLoad,
  iterateLoadState,
  getLoad,
  updateLoad,
  getShipingInfo,
} = require('../controllers/loadController');
const { authMiddleware } = require('../middleware/authMiddleware');
const { shipperMiddleware } = require('../middleware/shipperMiddleware');
const { driverMiddleware } = require('../middleware/driverMiddleware');

const router = express.Router();

router.use(authMiddleware);

router.get('/', getLoads);

router.post('/', shipperMiddleware, addLoad);

router.delete('/:id', shipperMiddleware, deleteLoad);

router.post('/:id/post', shipperMiddleware, postLoad);

router.get('/active', driverMiddleware, getActiveLoad);

router.patch('/active/state', driverMiddleware, iterateLoadState);

router.get('/:id', getLoad);

router.put('/:id', shipperMiddleware, updateLoad);

router.get('/:id/shipping_info', shipperMiddleware, getShipingInfo);

module.exports = {
  loadRouter: router,
};
