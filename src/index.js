const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const { authRouter } = require('./routers/authRouter');
const { userRouter } = require('./routers/userRouter');
const { truckRouter } = require('./routers/truckRouter');
const { loadRouter } = require('./routers/loadRouter');

const app = express();

mongoose.connect(
  'mongodb+srv://JuuTruskova:MTWZ0smf1nUdjZTU@yuliiatruskovamongodbcl.njldnpm.mongodb.net/myAppUber?retryWrites=true&w=majority',
);

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    app.listen(8080);

    console.log('App started on port 8080');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
