const { User } = require('../models/User');

const driverMiddleware = async (req, res, next) => {
    const { userId } = req.user;
    const user = await User.findById(userId);

    if (!user) {
        return res.status(404).json({
            message: "User not found"
        });
    }

    if (user.role !== "DRIVER") {
        return res.status(400).json({
            message: "User should have DRIVER role to perform this action"
        })
    }

    next();
};

module.exports = {
    driverMiddleware
}
