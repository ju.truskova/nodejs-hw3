const { User } = require('../models/User');

const shipperMiddleware = async (req, res, next) => {
    const { userId } = req.user;
    const user = await User.findById(userId);

    if (!user) {
        return res.status(404).json({
            message: "User not found"
        });
    }

    if (user.role !== "SHIPPER") {
        return res.status(400).json({
            message: "User should have SHIPPER role to perform this action"
        })
    }

    next();
}

module.exports = {
    shipperMiddleware
}
